# Knight's Travails

A happy-path solution to the Knight's Travails problem.

## Requirements

* Node 12.x

## Setup

Run the following to install dependencies:

```
npm install
```

## Testing

Run the tests using:

```
npm test
```

## Run locally

To run the solution against the sample input, use the command:

```
npm start
```

## To do

If I had more time, I would implement the following error cases:

* When the input isn't valid JSON
* When the the first event isn't a RESET event
* When there is a RESET event not at the start
* When the event is not a RESET or MOVE event
* When the event doesn't have a type or data key
* When the event data isn't in algebraic chess notation
