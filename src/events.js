import calculateShortestPath from "./shortest-path";

const processEvents = events => {
    const moves = [];
    let currentPosition;

    events.forEach(event => {
        switch (event.type) {
            case "RESET":
                currentPosition = event.data;
                break;
            case "MOVE":
                const path = calculateShortestPath(currentPosition, event.data);
                moves.push(path);
                currentPosition = event.data;
        }
    });

    return moves;
};

export default processEvents;
