import knightsTravails from "./knights-travails";
import processEvents from "./events";

jest.mock('./events');

describe('knightsTravails', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should process the parsed input string', () => {
        processEvents.mockReturnValue([]);
        knightsTravails('[{ "type": "RESET", "data": "A1" }]');
        expect(processEvents).toHaveBeenCalledWith([{type: "RESET", data: "A1"}])
    });

    it('should return an empty string when processEvents returns an empty list', () => {
        processEvents.mockReturnValue([]);
        const result = knightsTravails("[]");
        expect(result).toEqual('')
    });

    it('should return the formatted result when processEvents returns a single move', () => {
        processEvents.mockReturnValue([["A1", "B2", "C3"]]);
        const result = knightsTravails("[]");
        expect(result).toEqual(`[A1] -> B2 -> C3`)
    });

    it('should return the formatted result when processEvents returns multiple moves', () => {
        processEvents.mockReturnValue([
            ["A1", "B2", "C3"],
            ["D4", "E5", "F6"]
        ]);
        const result = knightsTravails("[]");
        expect(result).toEqual(`[A1] -> B2 -> C3\n[D4] -> E5 -> F6`)
    });
});