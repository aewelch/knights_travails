import fs from 'fs';
import knightsTravails from "./knights-travails";

fs.readFile('sample-input.json', (err, data) => {
    if (err) throw err;
    console.log(knightsTravails(data));
});
