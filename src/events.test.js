import processEvents from "./events";
import calculateShortestPath from "./shortest-path";

jest.mock('./shortest-path');

describe('processEvents', () => {
    beforeEach(() => {
        jest.clearAllMocks();
        calculateShortestPath.mockImplementation((start, target) => [start, target])
    });

    it('should return an empty list when events is empty', () => {
        const result = processEvents([]);
        expect(result).toEqual([]);
    });

    it('should return an empty list when only the reset event is provided', () => {
        const result = processEvents([{type: "RESET", data: "A1"}]);
        expect(result).toEqual([]);
    });

    it('should return the shortest paths', () => {
        const result = processEvents([
            {type: "RESET", data: "A1"},
            {type: "MOVE", data: "C3"},
            {type: "MOVE", data: "E5"},
            {type: "MOVE", data: "G7"}
        ]);

        expect(result).toEqual([
            ["A1", "C3"],
            ["C3", "E5"],
            ["E5", "G7"]
        ]);
        expect(calculateShortestPath).toHaveBeenCalledWith("A1", "C3");
        expect(calculateShortestPath).toHaveBeenCalledWith("C3", "E5");
        expect(calculateShortestPath).toHaveBeenCalledWith("E5", "G7");
    });
});