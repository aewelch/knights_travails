import processEvents from "./events";

const formatOutput = result => result.map(moves => {
    const [first, ...rest] = moves;
    return [`[${first}]`, ...rest].join(' -> ')
}).join('\n');

const knightsTravails = (input) => {
    const events = JSON.parse(input);
    const result = processEvents(events);
    return formatOutput(result)
};

export default knightsTravails;
