import calculateShortestPath, {getPath, getPossibleMoves} from "./shortest-path";

describe('calculateShortestPath', () => {
    it('should return a path of length one when initial and target positions are the same', () => {
        const moves = calculateShortestPath('A1', 'A1');
        expect(moves).toEqual(['A1'])
    });

    it('should return the path from the initial to the target position', () => {
        const moves = calculateShortestPath('A1', 'C5');
        expect(moves).toEqual(['A1', 'B3', 'C5'])
    });
});

describe('getPossibleMoves', () => {
    it('should return all positions that a knight can move to', () => {
        const moves = getPossibleMoves('E5');
        expect(moves).toHaveLength(8);
        expect(moves).toContain('F3');
        expect(moves).toContain('F7');
        expect(moves).toContain('D3');
        expect(moves).toContain('D7');
        expect(moves).toContain('C4');
        expect(moves).toContain('C6');
        expect(moves).toContain('G4');
        expect(moves).toContain('G6');
    });

    describe('positions on the board edges', () => {
        it('should only return valid moves at A1', () => {
            const moves = getPossibleMoves('A1');
            expect(moves).toHaveLength(2);
            expect(moves).toContain('C2');
            expect(moves).toContain('B3');
        });

        it('should only return valid moves at H1', () => {
            const moves = getPossibleMoves('H1');
            expect(moves).toHaveLength(2);
            expect(moves).toContain('F2');
            expect(moves).toContain('G3');
        });

        it('should only return valid moves at A8', () => {
            const moves = getPossibleMoves('A8');
            expect(moves).toHaveLength(2);
            expect(moves).toContain('B6');
            expect(moves).toContain('C7');
        });

        it('should only return valid moves at H8', () => {
            const moves = getPossibleMoves('H8');
            expect(moves).toHaveLength(2);
            expect(moves).toContain('F7');
            expect(moves).toContain('G6');
        });
    });
});

describe('getPath', () => {
    it('should return a list of one position when there are no previous moves', () => {
        const move = {
            position: 'E',
            previous: null
        };

        const result = getPath(move);

        expect(result).toEqual(['E'])
    });

    it('should return a list of positions following the previous key', () => {
        const move = {
            position: 'E',
            previous: {
                position: 'D',
                previous: {
                    position: 'C',
                    previous: {
                        position: 'B',
                        previous: {
                            position: 'A',
                            previous: null
                        }
                    }
                }
            }
        };

        const result = getPath(move);

        expect(result).toEqual(['A', 'B', 'C', 'D', 'E'])
    });
});
