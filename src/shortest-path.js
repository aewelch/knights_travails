const knightsMoves = [
    {dx: -2, dy: -1},
    {dx: -2, dy: 1},
    {dx: -1, dy: -2},
    {dx: -1, dy: 2},
    {dx: 1, dy: -2},
    {dx: 1, dy: 2},
    {dx: 2, dy: -1},
    {dx: 2, dy: 1}
];

export const getPossibleMoves = (position) => {
    const coordinates = position.split('');

    return knightsMoves
        .map(({dx, dy}) => ({
            x: String.fromCharCode(coordinates[0].charCodeAt(0) + dx),
            y: parseInt(coordinates[1], 10) + dy
        }))
        .filter(({x, y}) => x >= 'A' && x <= 'H' && y >= 1 && y <= 8)
        .map(({x, y}) => x + y);
};

export const getPath = (move) =>
    move.previous
        ? [...getPath(move.previous), move.position]
        : [move.position];

// Uses a breadth-first search (https://en.wikipedia.org/wiki/Breadth-first_search)
const calculateShortestPath = (initialPosition, targetPosition) => {
    const queue = [];
    const visited = {};
    let current = {position: initialPosition, previous: null};

    while (current.position !== targetPosition) {
        getPossibleMoves(current.position)
            .filter(move => !visited[move])
            .forEach(move => queue.push({position: move, previous: current}));

        visited[current.position] = true;
        current = queue.shift();
    }

    return getPath(current);
};

export default calculateShortestPath;